package org.example;

public class Main {
    public static void main(String[] args) {
    Lorry Ford = new Lorry();
    SportCar Ferrari = new SportCar();
        Ford.start();
        Ford.stop();
        Ford.openTrunk();
        Ford.closeTrunk();
        Ford.SportDrivingMode();
        Ford.urbanDrivingMode();
        Ford.leftTurnSignal();
        Ford.rightTurnSignal();
        Ford.turnOffTheBacklight();
        Ford.turnOnTheBacklight();
        Ford.turnOffTheHeadlight();
        Ford.turnOnTheHeadlight();
        Ferrari.start();
        Ferrari.stop();
        Ferrari.openTrunk();
        Ferrari.closeTrunk();
        Ferrari.SportDrivingMode();
        Ferrari.urbanDrivingMode();
        Ferrari.leftTurnSignal();
        Ferrari.rightTurnSignal();
        Ferrari.turnOffTheBacklight();
        Ferrari.turnOnTheBacklight();
        Ferrari.turnOffTheHeadlight();
        Ferrari.turnOnTheHeadlight();

    }
}